package nl.teuno.customerstatementprocessor.reader;

import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestSimplifiedMT940RecordXMLReader {

    @Test
    void readValidXML() {
        Path resourceDirectory = Paths.get("src", "test", "resources", "records.xml");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        var reader = new SimplifiedMT940RecordXMLReader();
        List<SimplifiedMT940Record> records = reader.read(absolutePath);
        assertThat(records.size()).isEqualTo(10);
    }
}
