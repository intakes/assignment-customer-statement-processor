package nl.teuno.customerstatementprocessor.processor;

import nl.garvelink.iban.IBAN;
import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSimplifiedMT940RecordProcessor {

    @ParameterizedTest
    @MethodSource("provideRecords")
    void testInvalidRecordsAreInvalid(SimplifiedMT940Record record, boolean isValid) {
        assertThat(record.isValid()).isEqualTo(isValid);
    }

    private static Stream<Arguments> provideRecords() {
        return Stream.of(
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test description"), true),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.43), BigDecimal.valueOf(12.44), "test description"), false),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(-2.44), BigDecimal.valueOf(7.56), "test description"), true),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(-2.43), BigDecimal.valueOf(12.44), "test description"), false),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(-10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(-7.56), "test description"), true),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(-10), BigDecimal.valueOf(2.43), BigDecimal.valueOf(12.44), "test description"), false),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(-10), BigDecimal.valueOf(-2.44), BigDecimal.valueOf(-12.44), "test description"), true),
                Arguments.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(-10), BigDecimal.valueOf(-2.43), BigDecimal.valueOf(12.44), "test description"), false)
        );
    }

    @Test
    void testUniqueReferences() {
        List<SimplifiedMT940Record> records = List.of(
                new SimplifiedMT940Record(1233L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test1 description"),
                new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test2 description"),
                new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test3 description")
        );

        SimplifiedMT940RecordProcessor processor = new SimplifiedMT940RecordProcessor();
        List<SimplifiedMT940Record> invalidRecords = processor.process(records);

        assertThat(invalidRecords.size()).isEqualTo(2);
        assertThat(invalidRecords.get(0).transactionReference()).isEqualTo(1234L);
        assertThat(invalidRecords.get(0).description()).isEqualTo("test2 description");
        assertThat(invalidRecords.get(1).description()).isEqualTo("test3 description");
    }

    @Test
    void testUniqueReferencesWithInvalidBalanceCalculation() {
        List<SimplifiedMT940Record> records = List.of(
                new SimplifiedMT940Record(1233L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test1 description"),
                new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test2 description"),
                new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test3 description"),
                new SimplifiedMT940Record(1235L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(3.44), BigDecimal.valueOf(12.44), "test4 description")
        );
        SimplifiedMT940RecordProcessor processor = new SimplifiedMT940RecordProcessor();
        List<SimplifiedMT940Record> invalidRecords = processor.process(records);

        assertThat(invalidRecords.size()).isEqualTo(3);

    }

}
