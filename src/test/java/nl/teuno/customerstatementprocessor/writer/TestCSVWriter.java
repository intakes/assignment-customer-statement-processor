package nl.teuno.customerstatementprocessor.writer;

import nl.garvelink.iban.IBAN;
import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestCSVWriter {

    @Test
    void testCSVWriter() throws IOException {
        List<SimplifiedMT940Record> records = List.of(new SimplifiedMT940Record(1234L, IBAN.valueOf("NL69ABNA0433647324"), BigDecimal.valueOf(10), BigDecimal.valueOf(2.44), BigDecimal.valueOf(12.44), "test2 description"));
        Path resourceDirectory = Paths.get("src", "test", "resources", "output.csv");
        String filelocation = resourceDirectory.toFile().getAbsolutePath();

        CSVReportWriter writer = new CSVReportWriter(filelocation);
        writer.write((records));

        long lines = Files.lines(resourceDirectory).count();
        assertThat(lines).isEqualTo(2);

        resourceDirectory.toFile().delete();
    }
}
