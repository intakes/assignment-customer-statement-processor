package nl.teuno.customerstatementprocessor.entity;

import nl.garvelink.iban.IBAN;

import java.math.BigDecimal;

public record SimplifiedMT940Record(Long transactionReference,
                                    IBAN iban,
                                    BigDecimal startBalance,
                                    BigDecimal mutation,
                                    BigDecimal endBalance,
                                    String description) {

    public boolean isValid() {
        return startBalance.add(mutation).equals(endBalance);
    }
}
