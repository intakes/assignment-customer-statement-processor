package nl.teuno.customerstatementprocessor.writer;

import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CSVReportWriter {
    private static Logger logger = LoggerFactory.getLogger(CSVReportWriter.class);

    private final String[] HEADERS = {"Reference", "Description"};
    private String filelocation = "";

    public CSVReportWriter(String filelocation) {
        this.filelocation = filelocation;
    }

    public void write(List<SimplifiedMT940Record> records) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(filelocation))) {

            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(HEADERS));

            for (SimplifiedMT940Record record : records) {
                csvPrinter.printRecord(record.transactionReference(), record.description());
            }
            csvPrinter.flush();

        } catch (IOException e) {
            logger.error("error in writing to " + filelocation, e);
            throw new RuntimeException(e);
        }
    }
}
