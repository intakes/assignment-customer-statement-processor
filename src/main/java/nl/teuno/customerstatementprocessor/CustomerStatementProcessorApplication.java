package nl.teuno.customerstatementprocessor;

import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import nl.teuno.customerstatementprocessor.processor.SimplifiedMT940RecordProcessor;
import nl.teuno.customerstatementprocessor.reader.SimplifiedMT940RecordCSVReader;
import nl.teuno.customerstatementprocessor.reader.SimplifiedMT940RecordReader;
import nl.teuno.customerstatementprocessor.reader.SimplifiedMT940RecordXMLReader;
import nl.teuno.customerstatementprocessor.writer.CSVReportWriter;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

public class CustomerStatementProcessorApplication {

    public static final SimplifiedMT940RecordProcessor processor = new SimplifiedMT940RecordProcessor();

    public static final Map<String, SimplifiedMT940RecordReader> readers = Map.ofEntries(
            new AbstractMap.SimpleEntry<String, SimplifiedMT940RecordReader>("xml", new SimplifiedMT940RecordXMLReader()),
            new AbstractMap.SimpleEntry<String, SimplifiedMT940RecordReader>("csv", new SimplifiedMT940RecordCSVReader()));

    public static void main(String[] args) {
        String outputLocation;
        String inputLocation;
        if (args.length == 2) {
            System.out.println(args[0]);
            System.out.println(args[1]);
            inputLocation = new File(args[0]).getAbsolutePath();
            outputLocation = new File(args[1]).getAbsolutePath();
        } else {
            throw new RuntimeException("The first argument should be the input path the second the output path");
        }

        String fileExtension = FilenameUtils.getExtension(inputLocation);
        SimplifiedMT940RecordReader reader = readers.get(fileExtension);
        if (reader == null){
            throw new RuntimeException("Only csv and XML format are supported");
        }
        CSVReportWriter writer = new CSVReportWriter(outputLocation);


        List<SimplifiedMT940Record> records = reader.read(inputLocation);
        List<SimplifiedMT940Record> invalidRecords = processor.process(records);
        writer.write((invalidRecords));
    }

}
