package nl.teuno.customerstatementprocessor.reader;


import nl.garvelink.iban.IBAN;
import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


public class SimplifiedMT940RecordXMLReader implements SimplifiedMT940RecordReader {

    private static Logger logger = LoggerFactory.getLogger(SimplifiedMT940RecordXMLReader.class);

    private final SAXBuilder builder = new SAXBuilder();


    @Override
    public List<SimplifiedMT940Record> read(String filelocation) {
        List<SimplifiedMT940Record> entityRecords = new ArrayList<>();

        try (Reader in = new FileReader(filelocation, StandardCharsets.UTF_8)) {
            Document doc = builder.build(in);
            List<Element> elements = doc.getRootElement().getChildren("record");

            for (Element record : elements) {
                logger.info("reading xml: %s".format(record.toString()));
                Long transactionReference = Long.parseLong(record.getAttribute("reference").getValue());
                IBAN accountNumber = IBAN.valueOf(record.getChild("accountNumber").getValue());
                String description = record.getChild("description").getValue();
                BigDecimal startBalance = new BigDecimal(record.getChild("startBalance").getValue());
                BigDecimal mutation = new BigDecimal(record.getChild("mutation").getValue());
                BigDecimal endBalance = new BigDecimal(record.getChild("endBalance").getValue());
                var entity = new SimplifiedMT940Record(transactionReference, accountNumber, startBalance, mutation, endBalance, description);
                entityRecords.add(entity);
            }
        } catch (IOException e) {
            logger.error(filelocation + " can't be found", e);
            throw new RuntimeException(e);
        } catch (JDOMException e) {
            logger.error("Element can't be parsed correctly", e);
            throw new RuntimeException(e);
        }
        return entityRecords;

    }
}
