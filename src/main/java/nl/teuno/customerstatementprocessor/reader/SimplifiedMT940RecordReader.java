package nl.teuno.customerstatementprocessor.reader;

import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;

import java.io.IOException;
import java.util.List;

public interface SimplifiedMT940RecordReader {

    List<SimplifiedMT940Record> read(String filelocation);
}
