package nl.teuno.customerstatementprocessor.reader;

import nl.garvelink.iban.IBAN;
import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class SimplifiedMT940RecordCSVReader implements SimplifiedMT940RecordReader {
    private static Logger logger = LoggerFactory.getLogger(SimplifiedMT940RecordCSVReader.class);

    private final String[] HEADERS = {"Reference", "Account Number", "Description", "Start Balance", "Mutation", "End Balance"};

    @Override

    public List<SimplifiedMT940Record> read(String filelocation) {
        List<SimplifiedMT940Record> entityRecords = new ArrayList<>();

        try (Reader in = new FileReader(filelocation, StandardCharsets.ISO_8859_1)) {
            Iterable<CSVRecord> records;

            records = CSVFormat.DEFAULT
                    .withHeader(HEADERS)
                    .withFirstRecordAsHeader()
                    .parse(in);

            for (CSVRecord record : records) {
                logger.info("reading csv: %s".format(record.toString()));
                Long transactionReference = Long.parseLong(record.get("Reference"));
                IBAN accountNumber = IBAN.valueOf(record.get("Account Number"));
                String description = record.get("Description");
                BigDecimal startBalance = new BigDecimal(record.get("Start Balance"));
                BigDecimal mutation = new BigDecimal(record.get("Mutation"));
                BigDecimal endBalance = new BigDecimal(record.get("End Balance"));
                var entity = new SimplifiedMT940Record(transactionReference, accountNumber, startBalance, mutation, endBalance, description);
                entityRecords.add(entity);
            }
        } catch (IOException e) {
            logger.error("error in parsing " + filelocation, e);
            throw new RuntimeException(e);
        }
        return entityRecords;
    }
}
