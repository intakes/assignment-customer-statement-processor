package nl.teuno.customerstatementprocessor.processor;

import nl.teuno.customerstatementprocessor.entity.SimplifiedMT940Record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class SimplifiedMT940RecordProcessor {


    public List<SimplifiedMT940Record> process(List<SimplifiedMT940Record> records) {
        List<SimplifiedMT940Record> invalidBalanceCalculation = getInvalidBalanceCalculation(records);

        List<SimplifiedMT940Record> duplicates = getDuplicateRecords(records);


        return Stream.concat(invalidBalanceCalculation.stream(), duplicates.stream()).toList();
    }

    private static List<SimplifiedMT940Record> getInvalidBalanceCalculation(List<SimplifiedMT940Record> records) {
        return records.stream()
                .filter(simplifiedMT940Record -> !simplifiedMT940Record.isValid())
                .toList();
    }

    private static List<SimplifiedMT940Record> getDuplicateRecords(List<SimplifiedMT940Record> records) {
        final Map<Long, List<SimplifiedMT940Record>> recordMap = new HashMap<>();

        for (SimplifiedMT940Record record : records) {
            List<SimplifiedMT940Record> item;
            if (recordMap.containsKey(record.transactionReference())) {
                item = recordMap.get(record.transactionReference());
            } else {
                item = new ArrayList<>();
                recordMap.put(record.transactionReference(), item);
            }
            item.add(record);
        }

        List<SimplifiedMT940Record> duplicates = new ArrayList<>();
        for (Map.Entry<Long, List<SimplifiedMT940Record>> set : recordMap.entrySet()) {
            if (set.getValue().size() > 1) {
                duplicates.addAll(set.getValue());
            }
        }
        return duplicates;
    }
}
