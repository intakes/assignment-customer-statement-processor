# Assignment Rabobank Customer Statement Processor

## Questions before

- Should they be unique only for the records in a file?
- Is a valid endbalance equal to startBalance + mutation = endBalance?
- In the end a report should be created. Which type of format is preferred to use for this?
- Should the end report be a file on the file system? If not what should it be?
- Can I assume for the assignment that the files are somewhere in the filesystem?
- Are there always this few records or is it outside this assignment a lot more and should I assume that amount?

---------------------------------------------

- What type of encoding is the file?
  file -bi records.csv == text/csv; charset=iso-8859-1

## Assumptions, because no answer and had to continue with something:

- All files are this type size
- Only look for duplicates in 1 file
- A valid balance calculation is: startBalance + mutation = endBalance
- It's a manual process (java jar) that started again for each run.
- We control the input and output location that we give in as input parameters.

## Process

Overall orchestration in CustomerStatementProcessorApplication main.

- Read
    - CSV (read via SimplifiedMT940RecordCSVReader)
    - XML (read via SimplifiedMT940RecordXMLReader)
- Process (SimplifiedMT940RecordProcessor.process)
    - Unique transaction (via SimplifiedMT940RecordProcessor.getDuplicateRecords)
    - Valid balance (SimplifiedMT940RecordProcessor.getInvalidBalanceCalculation)
- Write
    - Write report of invalid records (CSVReportWriter.write)

## Maven Plugins

- code coverage (jacoco-maven-plugin)
- dependency vulnerabilities (dependency-check-maven)
- static analyzer (spotbugs-maven-plugin and findsecbugs-plugin)
- license checker (license-maven-plugin)

## Build application

`./mvnw  clean install`

## Run the application

`java -jar customer-statement-processor-0.0.1-SNAPSHOT.jar  input_file_path output_file_path.csv`